.. RBF documentation master file, created by
   sphinx-quickstart on Thu Aug 13 14:51:24 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RBF's documentation!
===================================

Radial Basis Functions (RBF) are used to interpolate scattered data. This class can be used for 'typical' RBF-interpolation and has possibilities for solving PDEs with RBFs.
The implementation is based on the Paper 'MATLAB-Programming for Kernel-Based Methods' by Robert Schaback. The standard RBF-types are implemented. 

Requirements:

RBF depends on the numpy and scipy package.

Contents:

.. toctree::
   :maxdepth: 2

   tutorial
   code

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

