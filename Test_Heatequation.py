from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from matplotlib.mlab import  bivariate_normal
from matplotlib import cm
from RBF import RBF
import numpy as np
import scipy as sp


def pm3(x,y,z):
    return np.array([(x[mi][m1],y[mi][m1],z[mi][m1]) for mi in range(len(x)) for m1 in range(len(x[mi]))])


#generate Points
XE,YE = np.meshgrid(np.arange(-3.0,3.0,0.2),np.arange(-3.0,3.0,0.2))
ZE = bivariate_normal(XE, YE, 1.0, 1.0, 0.0, 0.0)
pointsz = pm3(XE,YE,ZE)

#interpolate
rbfi = RBF(points = pointsz[:,:2], values = pointsz[:,2], function='gaussian', scale = 0.5)
newvals = rbfi(pointsz[:,:2])
accoeffs = rbfi.coeffs

#laplace-matrix
dop, dip = pointsz.shape
dmat = rbfi.getdistmat(pointsz[:,:2],pointsz[:,:2])
rbfmat = rbfi.frbf(dmat,0)
lapmat = (dip*rbfi.frbf(dmat,1)+2*dmat*rbfi.frbf(dmat,2))

#variables
a = 0.5
Nt = 100
#ht = h**2*h**2/( 2*a*(h**2+h**2) )
ht = 0.005
data = []
u = newvals
data.append(u)
print(accoeffs,np.dot(lapmat,accoeffs))


#plot

for t in range(Nt):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    data.append(data[t]+a*ht*(np.dot(lapmat,accoeffs)))
    ax.plot_trisurf(pointsz[:,0], pointsz[:,1], np.dot(rbfmat,data[t]), cmap = cm.jet)
    ax.set_zlim3d(bottom=-0.1,top=12)
    #print(data[t])
    plt.savefig('pics/test{}.png'.format(t))
    plt.close()

"""
alternative:
for t in range(Nt):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    newcoeffs = np.linalg.solve(rbfmat,data[t])
    data.append(data[t]+a*ht*(np.dot(lapmat,newcoeffs)))
    ax.plot_trisurf(pointsz[:,0], pointsz[:,1], data[t], cmap = cm.jet)
    #print(data[t])
    plt.savefig('pics/test{}.png'.format(t))
"""