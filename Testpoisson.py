#-------------------------------------------------------------------------------
# Name:        TestPoisson
# Purpose:     Evaluate PoissonProblem with straight collocation
#
# Author:      Joachim
#
# Created:     26.08.2015
# Copyright:   (c) Joachim 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from RBF import RBF


def u5(x,y):
    return np.sin(np.pi*(x**2+y**2))

def lapu5(x,y):
    return np.sin(np.pi*(x.transpose()**2+y.transpose()**2))*(-4.0*np.pi*(x.transpose()**2+y.transpose()**2))

#interpolation points


def pm3(x,y,z):
    return np.array([(x[mi][m1],y[mi][m1],z[mi][m1]) for mi in range(len(x)) for m1 in range(len(x[mi]))])

#generate interpolation points in unit circle
newradius = np.sqrt(np.random.random((80,)));
newwinkel = 2* np.pi * np.random.random((80,))


arrcoordi = np.array([(newradius[m]*np.cos(newwinkel[m]),newradius[m]*np.sin(newwinkel[m])) for m in range(len(newwinkel))])
arrwinkel = 0.0 + (2*np.pi)*np.random.rand(30,1).transpose()[0]
pminterpolatei = np.array([(newradius[m]*np.cos(newwinkel[m]),newradius[m]*np.sin(newwinkel[m])) for m in range(len(newwinkel))])
pminterpolateb = np.array([(np.cos(n),np.sin(n)) for n in arrwinkel])


#generate values for interpolation
valinterpolateb = u5(pminterpolateb[:,0],pminterpolateb[:,1])
valinterpolatei = lapu5(pminterpolatei[:,0],pminterpolatei[:,1])
pminterpolateall = np.concatenate((pminterpolateb,pminterpolatei))
valinterpolateall = np.concatenate((valinterpolateb,valinterpolatei))


#generate evaluation points
pmevaluatei = np.array([(m*np.cos(n), m*np.sin(n)) for n in np.arange(0.,2*np.pi, 2*np.pi/30.) for m in np.arange(0.,1.0,0.2)])
pmevaluateb = np.array([(np.cos(n),np.sin(n)) for n in np.arange(0.,2*np.pi, 2*np.pi/30.)])
valevaluate = np.concatenate((u5(pmevaluateb[:,0],pmevaluateb[:,1]),lapu5(pmevaluatei[:,0],pmevaluatei[:,1])))
pmevaluateall = np.concatenate((pmevaluateb,pmevaluatei))



prbf2 = RBF(function ='power', par = 5.0)

#create distance matrices and solve the equation system via kansa's straight collocation method
#(for more, see Fornberg and larsson: https://amath.colorado.edu/faculty/fornberg/Docs/el_bf.pdf)
distmati = prbf2.getdistmat(pminterpolatei,pminterpolateall)
distmatb = prbf2.getdistmat(pminterpolateb,pminterpolateall)
evalmatb = prbf2.frbf(distmatb,0)
dop, dip = pminterpolatei.shape
evalmati = (dip*prbf2.frbf(distmati,1)+2*distmati*prbf2.frbf(distmati,2))
evalmatall = np.concatenate((evalmatb,evalmati))
coeffs = np.linalg.solve(evalmatall, valinterpolateall)
print(evalmatall.shape, valinterpolateall.shape)



#new distance matrices for reconstruction
distmatevali = prbf2.getdistmat(pmevaluatei, pminterpolateall)
distmatevalb = prbf2.getdistmat(pmevaluateb, pminterpolateall)

newdip = pmevaluatei.shape[1]
newevalmatb = prbf2.frbf(distmatevalb,0)
newevalmati = prbf2.frbf(distmatevali,0)
newevalmat = np.concatenate((newevalmatb,newevalmati))
newvals = np.dot(newevalmat, coeffs)

#plot
fig = plt.figure()
ax = fig.add_subplot(221)
ax.set_title('interpolation points')
ax.plot(arrcoordi[:,0], arrcoordi[:,1], '.', color = 'blue')
ax.plot(pminterpolateb[:,0],pminterpolateb[:,1], '.', color = 'red')
ax = fig.add_subplot(222, projection ='3d')
ax.set_title('given function')
ax.plot_trisurf(pminterpolateall[:,0],pminterpolateall[:,1],u5(pminterpolateall[:,0],pminterpolateall[:,1]))
ax = fig.add_subplot(223, projection ='3d')
ax.set_title('reconstructed function')
ax.plot_trisurf(pmevaluateall[:,0],pmevaluateall[:,1],newvals)
ax = fig.add_subplot(224, projection ='3d')
ax.set_title('error')
ax.plot_trisurf(pmevaluateall[:,0],pmevaluateall[:,1],u5(pmevaluateall[:,0],pmevaluateall[:,1])-newvals)
plt.show()

