Example on how to use RBF
=========================

This is a short example about how to use RBF for interpolating a bivariate gaussian distribution in :math:`[-3,3]^2`.


At first import RBF and numpy for generate the point matrix. matplotlib is needed for plotting the results.


.. literalinclude:: Test.py
	:language: python
	:lines: 1-7



Then define a function for generating a point matrix of given coordinate matrices

.. literalinclude:: Test.py
	:language: python
	:lines: 10,11

Now generate random interpolation points by applying the elements of a random 2-dimensional array as axis 
of a 2-d meshgrid on :math:`[-3,3]^2`. Calculate the z-values of the given points via the bivariate-normal function. Create a 
point matrix with 3 columns.

.. literalinclude:: Test.py
	:language: python
	:lines: 15-21

Now initialize the RBF with the given interpolation points (x- and y-values, the 2 first columns of the point matrix), their z-values (the point matrices 2nd column),
use the gaussian function and scale with 0.5 to find the coefficient array.

.. literalinclude:: Test.py
	:language: python
	:lines: 24

Now generate a x-y-z Point set, which is much finer than the interpolation point set, to evaluate the solution. 

.. literalinclude:: Test.py
	:language: python
	:lines: 28-31

Reconstruct the function values of the XE-YE-Point Matrix by using the coefficients calculated with the interpolation points.

.. literalinclude:: Test.py
	:language: python
	:lines: 34-35

Now plot the interpolation points, the given function, the reconstructed function and the error. To get the same dimensions 
for vals1-coordinates as ZE-coordinates, you have to reshape vals1 with the shape of XE or YE. See the results below.

.. literalinclude:: Test.py
	:language: python
	:lines: 38-51

.. image:: plotRges.png
